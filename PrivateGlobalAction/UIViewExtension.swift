//
//  UIViewExtension.swift
//  PrivateGlobalAction
//
//  Created by gls on 2018. 6. 29..
//  Copyright © 2018년 Dongs. All rights reserved.
//

import UIKit

public extension UIView {
    /// UIView 를 이미지로 변환(보이는 그대로)
    ///
    /// - Parameter view: 변환할 view
    /// - Returns: 변환된 이미지
    func image(with view: UIView!) -> UIImage! {
        UIGraphicsBeginImageContextWithOptions(view.bounds.size, view.isOpaque, 0)
        view.layer.render(in: UIGraphicsGetCurrentContext()!)
        
        let img = UIGraphicsGetImageFromCurrentImageContext()
        
        UIGraphicsEndImageContext()
        
        return img
    }

    /// SubView 존재할때 가장 하단에 위치한 뷰 포인트
    ///
    /// - Returns: 가장 하단의 포인트
    func subviewBottom() -> CGFloat {
        var bottomPoint : CGFloat = 0
        for inView in self.subviews {
            let bottom = inView.frame.size.height + inView.frame.origin.y
            if bottom > bottomPoint {
                bottomPoint = bottom
            }
        }
        return bottomPoint
    }

    
    /// 화면에 효과 부여(뷰가 나타나거나 사라질때 애니메이션 적용)
    ///
    /// - Parameters:
    ///   - tType: Up, Bottom, Right, Left, Fade
    func addAnimation(transitionType tType: String!) {
        let transition = CATransition()
        transition.duration = 3/4
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeIn)
        
        transition.type = CATransitionType.push
        if tType.caseInsensitiveCompare("left") == .orderedSame {
            transition.subtype = CATransitionSubtype.fromRight
        }else if tType.caseInsensitiveCompare("right") == .orderedSame {
            transition.subtype = CATransitionSubtype.fromLeft
        }else if tType.caseInsensitiveCompare("bottom") == .orderedSame {
            transition.subtype = CATransitionSubtype.fromBottom
        }else if tType.caseInsensitiveCompare("up") == .orderedSame {
            transition.subtype = CATransitionSubtype.fromTop
        }else if tType.caseInsensitiveCompare("fade") == .orderedSame {
            transition.type = CATransitionType.fade
        }else if tType.caseInsensitiveCompare("flip") == .orderedSame {
            transition.type = CATransitionType.reveal
        }
        
        self.layer.add(transition, forKey: nil)
    }
    
    /// 애니메이션 효과 적용(Alert 처럼 띄용)
    ///
    func setAnimationLikeAlert() {
        let animation : CAKeyframeAnimation = CAKeyframeAnimation(keyPath: "transform")
        
        let scale1 = CATransform3DMakeScale(0.9, 0.9, 1)
        let scale2 = CATransform3DMakeScale(1.1, 1.1, 1)
        let scale3 = CATransform3DMakeScale(1.0, 1.0, 1)
        
        
        let frameValues = [
            NSValue(caTransform3D : scale1),
            NSValue(caTransform3D : scale2),
            NSValue(caTransform3D : scale3),
            ]
        animation.values = frameValues
        
        let frameTimes = [
            NSNumber(floatLiteral: 0),
            NSNumber(floatLiteral: 0.4),
            NSNumber(floatLiteral: 1.0),
            ]
        animation.keyTimes = frameTimes
        
        animation.fillMode = CAMediaTimingFillMode.forwards
        animation.isRemovedOnCompletion = true
        animation.duration = 0.5
        
        self.layer.add(animation, forKey: "popup")
    }
    
    
    /// 뷰 회전
    ///
    /// - Parameters:
    ///   - degree: 각도
    func setRotation(degree: CGFloat) {
        let trans = CGAffineTransform(rotationAngle: CGFloat(degreessToRadians(Double(degree))))
        self.transform = trans
    }
    
    /// 뷰 사이즈 조정
    ///
    /// - Parameters:
    ///   - scale: 0 - 1.0
    func setScale(scale: CGFloat) {
        let trans = CGAffineTransform(scaleX: scale, y: scale)
        self.transform = trans
    }
    func setScaleWidth(scale: CGFloat) {
        let trans = CGAffineTransform(scaleX : scale, y: 1.0)
        self.transform = trans
    }
    func setScaleHeight(scale: CGFloat) {
        
        let trans = CGAffineTransform(scaleX : 1.0, y: scale)
        self.transform = trans
    }

    
    /// 외곽선 추가
    ///
    /// - Parameters:
    ///   - borderColor: 색상
    ///   - borderWidth: 넓이
    ///   - borderDirection: BorderDirection
    func addBorderAutoLayout(borderColor : UIColor, borderWidth : Float, borderDirection : [BorderDirection] = [.all]) {
        
        let top = {//top
            let border = UIView()
            border.backgroundColor = borderColor
            border.tag = 2934
            
            self.addSubview(border)
            
            border.snp.makeConstraints({ (make) in
                make.left.equalToSuperview()
                make.right.equalToSuperview()
                make.top.equalToSuperview()
                make.height.equalTo(borderWidth)
            })
        }
        
        
        let bottom = {//top
            let border = UIView()
            border.backgroundColor = borderColor
            border.tag = 2934
            
            self.addSubview(border)
            
            border.snp.makeConstraints({ (make) in
                make.left.equalToSuperview()
                make.bottom.equalToSuperview()
                make.right.equalToSuperview()
                make.height.equalTo(borderWidth)
            })
        }
        
        let left = {//top
            let border = UIView()
            border.backgroundColor = borderColor
            border.tag = 2934
            
            self.addSubview(border)
            
            border.snp.makeConstraints({ (make) in
                make.left.equalToSuperview()
                make.bottom.equalToSuperview()
                make.top.equalToSuperview()
                make.width.equalTo(borderWidth)
            })
        }
        
        let right = {//top
            let border = UIView()
            border.backgroundColor = borderColor
            border.tag = 2934
            
            self.addSubview(border)
            
            border.snp.makeConstraints({ (make) in
                make.bottom.equalToSuperview()
                make.right.equalToSuperview()
                make.top.equalToSuperview()
                make.width.equalTo(borderWidth)
            })
        }
        
        if borderDirection.contains(.all) {
            left()
            right()
            top()
            bottom()
        }
        if borderDirection.contains(.left) {
            left()
        }
        if borderDirection.contains(.right) {
            right()
        }
        if borderDirection.contains(.top) {
            top()
        }
        if borderDirection.contains(.bottom){
            bottom()
        }
        
    }
    
    
    /// 외곽선 삭제(tag 2934)
    func removeBorderAutoLayout() {
        for inView in self.subviews {
            if inView.tag == 2934 {
                inView.removeFromSuperview()
            }
        }
    }

    
    
}

