//
//  UIScrollViewExtension.swift
//  PrivateGlobalAction
//
//  Created by gls on 2018. 6. 29..
//  Copyright © 2018년 Dongs. All rights reserved.
//

import UIKit

public extension UIScrollView {

    /// ScrollView.contentSize 배치된 크기로 가져오기
    ///
    /// - Parameter scrollView: 계산할 ScrollView
    /// - Returns: CGSize
    func getScrollViewAutoContentSizeFit() -> CGSize
    {
        var contentRect : CGRect = CGRect.zero
        
        for view: UIView in self.subviews {
            contentRect = contentRect.union(view.frame)
        }
        
        return contentRect.size;
    }
    
    /// ScrollView.contentSize 배치된 크기로 가져오기(높이값만 계산)
    ///
    /// - Parameter scrollView: 계산할 ScrollView
    /// - Returns: CGSize.height
    func getScrollViewAutoContentSizeFitOnlyHeight() -> CGSize
    {
        var contentRect : CGRect = CGRect.zero
        
        for view: UIView in self.subviews {
            contentRect = contentRect.union(view.frame)
        }
        return CGSize(width: self.frame.width, height: contentRect.size.height);
    }
    
    
    /// ScrollView 에 배치된 suview 로 contentsize 조정
    ///
    /// - Parameter buffer: 버퍼
    func updateScrollViewContentSize(_ buffer : Int = 0) {
        var lowestPointX: CGFloat = 0.0
        var restoreHorizontal: Bool = false
        var restoreVertical: Bool = false
        if self.responds(to: #selector(getter: self.showsHorizontalScrollIndicator)) && self.responds(to: #selector(getter: self.showsVerticalScrollIndicator)) {
            if (self.showsHorizontalScrollIndicator) {
                restoreHorizontal = true
                self.showsHorizontalScrollIndicator = false
            }
            if (self.showsVerticalScrollIndicator) {
                restoreVertical = true
                self.showsVerticalScrollIndicator = false
            }
        }
        for subView: UIView in self.subviews {
            if !subView.isHidden {
                let maxX: CGFloat = subView.frame.maxX
                if maxX > lowestPointX {
                    lowestPointX = maxX
                }
            }
        }
        if self.responds(to: #selector(getter: self.showsHorizontalScrollIndicator)) && self.responds(to: #selector(getter: self.showsVerticalScrollIndicator)) {
            if restoreHorizontal {
                self.showsHorizontalScrollIndicator = true
            }
            if restoreVertical {
                self.showsVerticalScrollIndicator = true
            }
        }
        
        var lowestPointY: CGFloat = 0.0
        restoreHorizontal = false
        restoreVertical = false
        if self.responds(to: #selector(getter: self.showsHorizontalScrollIndicator)) && self.responds(to: #selector(getter: self.showsVerticalScrollIndicator)) {
            if (self.showsHorizontalScrollIndicator) {
                restoreHorizontal = true
                self.showsHorizontalScrollIndicator = false
            }
            if (self.showsVerticalScrollIndicator) {
                restoreVertical = true
                self.showsVerticalScrollIndicator = false
            }
        }
        for subView: UIView in self.subviews {
            if !subView.isHidden && subView.tag != __GlobalActionIgnoreTag {
                let maxY: CGFloat = subView.frame.maxY
                if maxY > lowestPointY {
                    lowestPointY = maxY
                }
            }
        }
        if self.responds(to: #selector(getter: self.showsHorizontalScrollIndicator)) && self.responds(to: #selector(getter: self.showsVerticalScrollIndicator)) {
            if restoreHorizontal {
                self.showsHorizontalScrollIndicator = true
            }
            if restoreVertical {
                self.showsVerticalScrollIndicator = true
            }
        }
        self.contentSize = CGSize(width: lowestPointX + CGFloat(buffer), height: lowestPointY + CGFloat(buffer))
    }
    
    
    /// ScrollView 에 배치된 suview 로 contentsize 조정(높이만)
    ///
    /// - Parameters:
    ///   - buffer: 버퍼
    func updateScrollViewContentHeight(_ buffer : Int = 0) {
        //헐 이건 스크롤 인디케이터도 포함;;
        //    CGRect contentRect = CGRectZero;
        //    for (UIView *view in self.subviews) {
        //        contentRect = CGRectUnion(contentRect, view.frame);
        //    }
        //    self.contentSize = CGSizeMake(0, contentRect.size.height);
        //이건 잘됨
        
        var lowestPoint: CGFloat = 0.0
        var restoreHorizontal: Bool = false
        var restoreVertical: Bool = false
        if self.responds(to: #selector(getter: self.showsHorizontalScrollIndicator)) && self.responds(to: #selector(getter: self.showsVerticalScrollIndicator)) {
            if (self.showsHorizontalScrollIndicator) {
                restoreHorizontal = true
                self.showsHorizontalScrollIndicator = false
            }
            if (self.showsVerticalScrollIndicator) {
                restoreVertical = true
                self.showsVerticalScrollIndicator = false
            }
        }
        for subView: UIView in self.subviews {
            if !subView.isHidden && subView.tag != __GlobalActionIgnoreTag {
                let maxY: CGFloat = subView.frame.maxY
                if maxY > lowestPoint {
                    lowestPoint = maxY
                }
            }
        }
        if self.responds(to: #selector(getter: self.showsHorizontalScrollIndicator)) && self.responds(to: #selector(getter: self.showsVerticalScrollIndicator)) {
            if restoreHorizontal {
                self.showsHorizontalScrollIndicator = true
            }
            if restoreVertical {
                self.showsVerticalScrollIndicator = true
            }
        }
        self.contentSize = CGSize(width: self.frame.size.width, height: lowestPoint + CGFloat(buffer))
    }
    
    
    /// ScrollView 에 배치된 suview 로 contentsize 조정(넓이만)
    ///
    /// - Parameters:
    ///   - buffer: 버퍼
    func updateScrollViewContentWidth(_ buffer : Int = 0) {
        //    CGRect contentRect = CGRectZero;
        //    for (UIView *view in self.subviews) {
        //        contentRect = CGRectUnion(contentRect, view.frame);
        //    }
        //    self.contentSize = CGSizeMake(contentRect.size.width, 0);
        var lowestPoint: CGFloat = 0.0
        var restoreHorizontal: Bool = false
        var restoreVertical: Bool = false
        if self.responds(to: #selector(getter: self.showsHorizontalScrollIndicator)) && self.responds(to: #selector(getter: self.showsVerticalScrollIndicator)) {
            if (self.showsHorizontalScrollIndicator) {
                restoreHorizontal = true
                self.showsHorizontalScrollIndicator = false
            }
            if (self.showsVerticalScrollIndicator) {
                restoreVertical = true
                self.showsVerticalScrollIndicator = false
            }
        }
        for subView: UIView in self.subviews {
            if !subView.isHidden && subView.tag != __GlobalActionIgnoreTag {
                let maxX: CGFloat = subView.frame.maxX
                if maxX > lowestPoint {
                    lowestPoint = maxX
                }
            }
        }
        if self.responds(to: #selector(getter: self.showsHorizontalScrollIndicator)) && self.responds(to: #selector(getter: self.showsVerticalScrollIndicator)) {
            if restoreHorizontal {
                self.showsHorizontalScrollIndicator = true
            }
            if restoreVertical {
                self.showsVerticalScrollIndicator = true
            }
        }
        if self.isPagingEnabled {
            //페이징이면 페이지 단위로 넓이 지정
            let margin = lowestPoint.truncatingRemainder(dividingBy: self.frame.size.width)
            if margin > 0 {
                self.contentSize = CGSize(width: lowestPoint + CGFloat(buffer) + (self.frame.size.width - margin), height: self.frame.size.height)
            }else{
                self.contentSize = CGSize(width: lowestPoint + CGFloat(buffer), height: self.frame.size.height)
            }
            
        }else{
            self.contentSize = CGSize(width: lowestPoint + CGFloat(buffer), height: self.frame.size.height)
        }
        
    }

    
    
    /// Holder 추가(자체적으로 동적인 넓이, 높이 계산이 안됨)
    func addScrollHolder() {
        let horizontal = UIView()
        horizontal.tag = __GlobalActionIgnoreTag
        let vertical = UIView()
        vertical.tag = __GlobalActionIgnoreTag
        
        self.addSubview(horizontal)
        horizontal.snp.makeConstraints { (making) in
            making.left.equalToSuperview()
            making.right.equalToSuperview()
            making.width.equalToSuperview()
            making.centerX.equalToSuperview()
            making.height.equalTo(0)
        }
        self.addSubview(vertical)
        vertical.snp.makeConstraints { (making) in
            making.left.equalToSuperview()
            making.top.equalToSuperview()
            making.bottom.equalToSuperview()
            making.height.equalToSuperview()
            making.centerY.equalToSuperview()
            making.width.equalTo(0)
        }
        
    }


}
