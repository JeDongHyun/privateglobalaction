//
//  URLSessionExtension.swift
//  PrivateGlobalAction
//
//  Created by gls on 2018. 7. 2..
//  Copyright © 2018년 Dongs. All rights reserved.
//

import UIKit

public extension URLSession {
    
    func sendSynchronousRequest(request: URLRequest, completionHandler: @escaping (Data?, HTTPURLResponse?, Error?) -> Void) {
        let semaphore = DispatchSemaphore(value: 0)
        let task = self.dataTask(with: request) { data, response, error in
            completionHandler(data, response as? HTTPURLResponse, error)
            semaphore.signal()
        }
        
        task.resume()
        semaphore.wait()
        //semaphore.wait(timeout: dispatch_time_t(DispatchTime.distantFuture))
    }
}
