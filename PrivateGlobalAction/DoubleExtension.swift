//
//  DoubleExtension.swift
//  PrivateGlobalAction
//
//  Created by gls on 2018. 6. 29..
//  Copyright © 2018년 Dongs. All rights reserved.
//

import UIKit

public extension Double {
    /// Rounds the double to decimal places value
    func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}
