//
//  StringExtension.swift
//  PrivateGlobalAction
//
//  Created by gls on 2018. 6. 29..
//  Copyright © 2018년 Dongs. All rights reserved.
//

import UIKit

public extension String {
    
    /// 정규 표현식
    ///
    /// - Parameter regex: 표현식
    /// - Returns: 매칭결과
    func matches(for regex: String) -> [String] {
        if regex == "" { return [] }
        do {
            let regex = try NSRegularExpression(pattern: regex)
            let nsString = self as NSString
            let results = regex.matches(in: self, range: NSRange(location: 0, length: nsString.length))
            return results.map { nsString.substring(with: $0.range)}
        } catch let error {
            print("invalid regex: \(error.localizedDescription)")
            return []
        }
    }
    
    /// 높이값 계산해 오기
    ///
    /// - Parameters:
    ///   - width: 넓이
    ///   - font: 폰트
    /// - Returns: 높이값 반환
    func heightWithConstrainedWidth(width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        
        return boundingBox.height
    }
    
    
    /// 넓이값 계산해 오기
    ///
    /// - Parameter font: 폰트
    /// - Returns: 넓이값
    func widthWithFont(font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: 9000, height: 100)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        
        return boundingBox.width
    }
    
    
    /// URL에서 파일이름 추출
    ///
    /// - Parameter url: 전체 url
    /// - Returns: 파일이름 반환
    func extractFileName() -> String! {
        let convert = NSString(string: self)
        
        let lastSlash = convert.range(of: "/", options: .backwards)
        
        if lastSlash.location != NSNotFound {
            return convert.substring(from: lastSlash.location+1)
        }else{
            return ""
        }
    }
    
    /// RGB 문자열 코드값으로 색상 반환
    ///
    /// - Parameter hexString: 코드값
    /// - Returns: 색상
    func convertToColor () -> UIColor {
        var cString:String = self.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    /// 이메일 형식 정규표현식 검사
    ///
    /// - Parameter candidate: 체크할 이메일 주소
    /// - Returns: 형식 검사 통과 여부
    func validateEmail() -> Bool {
        let emailTest = NSPredicate(format: "SELF MATCHES %@", "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}")
        return emailTest.evaluate(with: self)
    }
    
    
    /// 전화번호 형식 정규표현식 검사(스마트폰 010 정책 적용)
    ///
    /// - Parameters:
    ///   - phoneNumber: 체크할 이메일 주소
    ///   - isHipon: '-'문자 존재 여부
    /// - Returns: 형식 검사 통과 여부
    func validatePhone(isHipon: Bool = false) -> Bool {
        var phoneRegex = ""
        if isHipon {
            phoneRegex = "^010-([0-9]{4})-([0-9]{4})"
        }else {
            phoneRegex = "^010([0-9]{4})([0-9]{4})"
        }
        
        let convert = NSString(string: self)
        let range = convert.range(of: phoneRegex, options: .regularExpression)
        
        if range.location != NSNotFound {
            return true
        }else{
            return false
        }
    }
    
    
    /// 전화번호 정규표현식 검사 예전 형식도 통과(스마트폰 010 정책 미적용, 주소록에서 가져올때 01x 번호들도 표시하기 위해)
    ///
    /// - Parameters:
    ///   - phoneNumber: 체크할 폰번호
    ///   - isHipon: '-' 존재여부
    /// - Returns: 형식 검사 통과 여부
    func validateOldPhone(isHipon: Bool) -> Bool {
        var phoneRegex = ""
        if isHipon {
            phoneRegex = "^01([0|1|6|7|8|9])-([0-9]{3,4})-([0-9]{4})"
        }else {
            phoneRegex = "^01([0|1|6|7|8|9])([0-9]{3,4})([0-9]{4})"
        }
        
        let convert = NSString(string: self)
        let range = convert.range(of: phoneRegex, options: .regularExpression)
        
        if range.location != NSNotFound {
            return true
        }else{
            return false
        }
    }
    
    
    /// 문자열에 공백만 존재하는지 여부 검사
    ///
    /// - Parameter checkString: 검사할 문자열
    /// - Returns: 검사 결과
    func validate() -> Bool {
        if self.count == 0 {
            return false
        }
        
        if self.trimmingCharacters(in: .whitespacesAndNewlines).count == 0 {
            return false
        }
        
        return true
    }
    
    
    /// Int 변환
    ///
    /// - Returns: 변환 Int 안되면 0
    func toInt() -> Int {
        return Int(self) ?? 0
    }
    /// Float 변환
    ///
    /// - Returns: 변환 Float 안되면 0
    func toFloat() -> Float {
        return Float(self) ?? 0.0
    }

    /// HTML 태그를 AttributedString 으로
    var html2AttributedString: NSAttributedString? {
        return Data(utf8).html2AttributedString
    }
    
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }

    subscript(pos: Int) -> String {
        precondition(pos >= 0, "character position can't be negative")
        return self[pos...pos]
    }
    subscript(range: Range<Int>) -> String {
        precondition(range.lowerBound >= 0, "range lowerBound can't be negative")
        let lowerIndex = index(startIndex, offsetBy: range.lowerBound, limitedBy: endIndex) ?? endIndex
        return String(self[lowerIndex..<(index(lowerIndex, offsetBy: range.count, limitedBy: endIndex) ?? endIndex)])
    }
    subscript(range: ClosedRange<Int>) -> String {
        precondition(range.lowerBound >= 0, "range lowerBound can't be negative")
        let lowerIndex = index(startIndex, offsetBy: range.lowerBound, limitedBy: endIndex) ?? endIndex
        return String(self[lowerIndex..<(index(lowerIndex, offsetBy: range.count, limitedBy: endIndex) ?? endIndex)])
    }
    
    typealias SimpleToFromRepalceList = [(fromSubString:String,toSubString:String)]
    
    // See http://stackoverflow.com/questions/24200888/any-way-to-replace-characters-on-swift-string
    //
    func simpleReplace( mapList:SimpleToFromRepalceList ) -> String
    {
        var string = self
        
        for (fromStr, toStr) in mapList {
            let separatedList = string.components(separatedBy: fromStr)
            if separatedList.count > 1 {
                string = separatedList.joined(separator: toStr)
            }
        }
        
        return string
    }
    
    func xmlSimpleUnescape() -> String
    {
        let mapList : SimpleToFromRepalceList = [
            ("&amp;",  "&"),
            ("&quot;", "\""),
            ("&#x27;", "'"),
            ("&#39;",  "'"),
            ("&#x92;", "'"),
            ("&#x96;", "-"),
            ("&gt;",   ">"),
            ("&lt;",   "<"),
            ("&nbsp;", " "),
            ("&#039;", "'")
        ]
        
        return self.simpleReplace(mapList: mapList)
    }
    
    func xmlSimpleEscape() -> String
    {
        let mapList : SimpleToFromRepalceList = [
            ("&",  "&amp;"),
            ("\"", "&quot;"),
            ("'",  "&#x27;"),
            (">",  "&gt;"),
            ("<",  "&lt;"),
            (" ", "&nbsp;")]
        
        return self.simpleReplace(mapList: mapList)
    }
    
    var isAlphanumeric: Bool {
        return !isEmpty && range(of: "[^a-zA-Z0-9]", options: .regularExpression) == nil
    }
    
    var isAlpha: Bool {
        return !isEmpty && range(of: "[^a-zA-Z]", options: .regularExpression) == nil
    }
    
    var isJapanese : Bool {
        /*
         Regex for matching ALL Japanese common & uncommon Kanji (4e00 – 9fcf) ~ The Big Kahuna!
         ([一-龯])
         
         Regex for matching Hirgana or Katakana
         ([ぁ-んァ-ン])
         
         Regex for matching Non-Hirgana or Non-Katakana
         ([^ぁ-んァ-ン])
         
         Regex for matching Hirgana or Katakana or basic punctuation (、。’)
         ([ぁ-んァ-ン\w])
         
         Regex for matching Hirgana or Katakana and random other characters
         ([ぁ-んァ-ン！：／])
         
         Regex for matching Hirgana
         ([ぁ-ん])
         
         Regex for matching full-width Katakana (zenkaku 全角)
         ([ァ-ン])
         
         Regex for matching half-width Katakana (hankaku 半角)
         ([ｧ-ﾝﾞﾟ])
         
         Regex for matching full-width Numbers (zenkaku 全角)
         ([０-９])
         
         Regex for matching full-width Letters (zenkaku 全角)
         ([Ａ-ｚ])
         
         Regex for matching Hiragana codespace characters (includes non phonetic characters)
         ([ぁ-ゞ])
         
         Regex for matching full-width (zenkaku) Katakana codespace characters (includes non phonetic characters)
         ([ァ-ヶ])
         
         Regex for matching half-width (hankaku) Katakana codespace characters (this is an old character set so the order is inconsistent with the hiragana)
         ([ｦ-ﾟ])
         
         Regex for matching Japanese Post Codes
         /^¥d{3}¥-¥d{4}$/
         /^¥d{3}-¥d{4}$|^¥d{3}-¥d{2}$|^¥d{3}$/
         
         Regex for matching Japanese mobile phone numbers (keitai bangou)
         /^¥d{3}-¥d{4}-¥d{4}$|^¥d{11}$/
         /^0¥d0-¥d{4}-¥d{4}$/
         
         Regex for matching Japanese fixed line phone numbers
         /^[0-9-]{6,9}$|^[0-9-]{12}$/
         /^¥d{1,4}-¥d{4}$|^¥d{2,5}-¥d{1,4}-¥d{4}$/
         */
        return !isEmpty && range(of: "[^ぁ-んァ-ン]", options: .regularExpression) == nil
    }
    
    var isKorean : Bool {
        
        return !isEmpty && range(of: "[^ㄱㄲㄴㄷㄸㄹㅁㅂㅃㅅㅆㅇㅈㅉㅊㅋㅌㅍㅎ]", options: .regularExpression) == nil
    }
    
    var hangul: String {
        get {
            let hangle = [
                ["ㄱ","ㄲ","ㄴ","ㄷ","ㄸ","ㄹ","ㅁ","ㅂ","ㅃ","ㅅ","ㅆ","ㅇ","ㅈ","ㅉ","ㅊ","ㅋ","ㅌ","ㅍ","ㅎ"],
                ["ㅏ","ㅐ","ㅑ","ㅒ","ㅓ","ㅔ","ㅕ","ㅖ","ㅗ","ㅘ","ㅙ","ㅚ","ㅛ","ㅜ","ㅝ","ㅞ","ㅟ","ㅠ","ㅡ","ㅢ","ㅣ"],
                ["","ㄱ","ㄲ","ㄳ","ㄴ","ㄵ","ㄶ","ㄷ","ㄹ","ㄺ","ㄻ","ㄼ","ㄽ","ㄾ","ㄿ","ㅀ","ㅁ","ㅂ","ㅄ","ㅅ","ㅆ","ㅇ","ㅈ","ㅊ","ㅋ","ㅌ","ㅍ","ㅎ"]
            ]
            guard let code = Unicode.Scalar(self[0])?.value else {
                return "0"
            }
            if code <= 44032 {
                return "0"
            }
            let reduce = code - 44032
            if reduce >= 0 && reduce < 11172 {
                let cho = reduce / 21 / 28
                return hangle[0][Int(cho)]
            }else {
                return "0"
            }
        }
    }

    
    /// 문자열을 Date 형으로 변환
    ///
    /// - Parameter dateStr: 문자열 YYYY-MM-DD 형식
    /// - Returns: 변환된 날짜형
    func toDate(type : StringToDateConvertingType) -> Date! {
        if self.validate() == false {
            return Date()
        }
        switch type {
        case .yyyymmdd:
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            
            let dateFormatter2 = DateFormatter()
            dateFormatter2.dateFormat = "yyyyMMdd"
            
            if let _ = dateFormatter.date(from: self) {
                //ok
            }else if let _ = dateFormatter2.date(from: self){
                //ok
            }else{
                return Date()
            }
            
            let convertDateStr = NSString(string : self)
            
            let stripStr = convertDateStr.replacingOccurrences(of: "-", with: "") as NSString
            let yearRange = NSMakeRange(0, 4)
            let year = stripStr.substring(with: yearRange) as NSString
            
            let monthRange = NSMakeRange(4, 2)
            let month = stripStr.substring(with: monthRange) as NSString
            
            let dayRange = NSMakeRange(6, 2)
            let day = stripStr.substring(with: dayRange)
            
            let dateString = NSString(format: "%@-%@-%@ 00:00:00 %@", year, month, day, Date().convertDate(toStr: "z"))
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss z"
            let returnDate = formatter.date(from: dateString as String)
            
            return returnDate
        case .yyyy:
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy"
            
            guard let _ = dateFormatter.date(from: self) else {
                return Date()
            }
            
            let convertDateStr = NSString(string : self)
            
            let stripStr = convertDateStr.replacingOccurrences(of: "-", with: "") as NSString
            let yearRange = NSMakeRange(0, 4)
            let year = stripStr.substring(with: yearRange) as NSString
            
            let dateString = NSString(format: "%@-01-01 00:00:00 %@", year, Date().convertDate(toStr: "z"))
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss z"
            let returnDate = formatter.date(from: dateString as String)
            
            return returnDate
        case .yyyyMMddHHmm:
            var convertDateStr = NSString(string: self)
            if self.count < 12 {
                convertDateStr = convertDateStr.appendingFormat("00000000000")
            }
            
            let stripStr = convertDateStr.replacingOccurrences(of: "-", with: "").replacingOccurrences(of: ":", with: "").replacingOccurrences(of: " ", with: "") as NSString
            
            let yearRange = NSMakeRange(0, 4)
            let year = stripStr.substring(with: yearRange) as NSString
            
            let monthRange = NSMakeRange(4, 2)
            let month = stripStr.substring(with: monthRange) as NSString
            
            let dayRange = NSMakeRange(6, 2)
            let day = stripStr.substring(with: dayRange)
            
            let hourRange = NSMakeRange(8, 2)
            let hour = stripStr.substring(with: hourRange) as NSString
            
            let minuteRange = NSMakeRange(10, 2)
            let minute = stripStr.substring(with: minuteRange)
            
            let dateString = NSString(format: "%@-%@-%@ %@:%@:00 %@", year, month, day, hour, minute, Date().convertDate(toStr: "z"))
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss z"
            let returnDate = formatter.date(from: dateString as String)
            
            return returnDate
//        default:
//            break
        }
        
    }
    
    func capitalizingFirstLetter() -> String {
        return prefix(1).capitalized + dropFirst()
    }
    
}


extension String: Error {}
