//
//  GlobalAction_Swift.swift
//  snojo2
//
//  Created by DongHyunJe on 2016. 4. 11..
//  Copyright © 2016년 DongHyunJe. All rights reserved.
//

import UIKit

import KeychainSwift
import WebKit
import QuartzCore
import UserNotifications
import SnapKit
//import RappleProgressHUD //https://github.com/rjeprasad/RappleProgressHUD
//import GestureRecognizerClosures

import Foundation
import SystemConfiguration

import CryptoSwift

import CoreLocation


/*
 Template:
 /// VIEWCONTROLLER TEMPLATE
 
 let _GS : UIStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
 public func _GET_VIEW(_ view_name : String) -> UIViewController {
 return _GS.instantiateViewController(withIdentifier: view_name)
 }
 
 var _VIEW_<#Name#> : <#VIEWCONTROLLERCLASSNAME#> {
 get {
 return _GET_VIEW("<#USINGNAME#>") as! <#VIEWCONTROLLERCLASSNAME#>
 }
 }
 
 */

public let _GA : GlobalAction_Swift = GlobalAction_Swift.sharedInstance()

let _AESKEY : String! = "privateglobalkk"
let _AESIV : String! = "kklabolgetavirpk"


/// ScrollView 넓이 계산시 Holder 로 잡아둔 컨트롤들은 제외 하려고
public let __GlobalActionIgnoreTag = 9393

open class GlobalAction_Swift: NSObject, CLLocationManagerDelegate {
    
    private static var __once: () = {
        StaticInstance.instance = GlobalAction_Swift()
    }()
    struct StaticInstance {
        static var dispatchToken: Int = 0
        static var instance: GlobalAction_Swift?
    }
    
    class func sharedInstance() -> GlobalAction_Swift {
        _ = GlobalAction_Swift.__once
        return StaticInstance.instance!
    }
    
    
    
    
    func SYSTEM_VERSION_EQUAL_TO(_ version: String) -> Bool {
        return UIDevice.current.systemVersion.compare(version,
                                                              options: NSString.CompareOptions.numeric) == ComparisonResult.orderedSame
    }
    
    func SYSTEM_VERSION_GREATER_THAN(_ version: String) -> Bool {
        return UIDevice.current.systemVersion.compare(version,
                                                              options: NSString.CompareOptions.numeric) == ComparisonResult.orderedDescending
    }
    
    func SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(_ version: String) -> Bool {
        return UIDevice.current.systemVersion.compare(version,
                                                              options: NSString.CompareOptions.numeric) != ComparisonResult.orderedAscending
    }
    
    func SYSTEM_VERSION_LESS_THAN(_ version: String) -> Bool {
        return UIDevice.current.systemVersion.compare(version,
                                                              options: NSString.CompareOptions.numeric) == ComparisonResult.orderedAscending
    }
    
    func SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(_ version: String) -> Bool {
        return UIDevice.current.systemVersion.compare(version,
                                                              options: NSString.CompareOptions.numeric) != ComparisonResult.orderedDescending
    }
    
    
    
    /// Push 권한 요청
    public func requestPushPermission() {
        // iOS 10 support
        if #available(iOS 10, *) {
            
            UNUserNotificationCenter.current().requestAuthorization(options:[.badge, .alert, .sound]){ (granted, error) in }
            UIApplication.shared.registerForRemoteNotifications()
        }
            // iOS 9 support
        else {
//        else if #available(iOS 9, *) {
            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.badge, .sound, .alert], categories: nil))
            UIApplication.shared.registerForRemoteNotifications()
        }
//            // iOS 8 support
//        else if #available(iOS 8, *) {
//            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.badge, .sound, .alert], categories: nil))
//            UIApplication.shared.registerForRemoteNotifications()
//        }
    }
    
    
    public func getAppDelegate() -> UIApplicationDelegate
    {
        return UIApplication.shared.delegate!
    }
    
    
    
    
    
    
    /// NSUserDefaults 사용 단순화
    ///
    /// - Parameters:
    ///   - defaultKey: Key 값
    ///   - keyChain: 키체인 사용 여부
    /// - Returns: NSUserDefaults 에 저장된 키와 매칭된 값(objectForKey:)
    public func getDefaultsKey(_ defaultKey: String!, keyChain : Bool = false, encrypt : Bool = true) -> Any! {
        
        
        if keyChain == false {
            let defaults = UserDefaults.standard
            if let value = defaults.object(forKey: defaultKey) {
                if encrypt {
                    do {
                        let aes = try AES(key: _AESKEY, iv: _AESIV)
                        if let valueString = value as? String  {
                            let encrypted = Array(hex: valueString)
                            let decrypted = try aes.decrypt(encrypted)
                            let decryptedString = String.init(data: Data(bytes: decrypted), encoding: .utf8)
                            return decryptedString
                        }else{
                            return value
                        }
                    } catch {
                        return value
                    }
                }else{
                    return value
                }
            }else {
                return ""
            }

        }else {
            let keychain = KeychainSwift()
            keychain.accessGroup = self._KEYCHAINGROUPID
            
            if self._KEYCHAINGROUPID.validate() {
                if let value = keychain.get(defaultKey) {
                    return value
                }else {
                    return ""
                }
            }else{
                assert(false, "키체인 그룹 설정")
            }
        }
        return ""
    }
    
    
    
    /// NSUserDefaults 사용 단순화
    ///
    /// - Parameters:
    ///   - defaultValue: 저장할 값 키체인의 경우 Bool, Data, String 만
    ///   - defaultKey: Key 값
    ///   - keyChain: 키체인 사용 여부
    public func setDefaultsValue(_ defaultValue: Any?, key defaultKey: String!, keyChain : Bool = false, encrypt : Bool = true) {
        
        if keyChain == false {
            let defaults = UserDefaults.standard
            if defaultValue == nil {
                defaults.removeObject(forKey: defaultKey)
            }else{
                if encrypt {
                    do {
                        let aes = try AES(key: _AESKEY, iv: _AESIV)
                        
                        if let value = defaultValue as? String {
                            let encrypted = try aes.encrypt(Array(value.utf8))
                            let encryptedHex = encrypted.toHexString()
                            defaults.set(encryptedHex, forKey: defaultKey)
                        }else {
                            defaults.set(defaultValue, forKey: defaultKey)
                        }
                    } catch {
                        defaults.set(defaultValue, forKey: defaultKey)
                    }
                }else{
                    defaults.set(defaultValue, forKey: defaultKey)
                }
                
            }
            defaults.synchronize()
            
        }else {
            let keychain = KeychainSwift()
            keychain.accessGroup = self._KEYCHAINGROUPID
            
            if self._KEYCHAINGROUPID.validate() {
                if defaultValue == nil {
                    
                    keychain.delete(defaultKey)
                }else{
                    if defaultValue is Bool {
                        keychain.set(defaultValue as! Bool, forKey: defaultKey)
                    }else if defaultValue is Data {
                        keychain.set(defaultValue as! Data, forKey: defaultKey)
                    }else if defaultValue is String {
                        keychain.set(defaultValue as! String, forKey: defaultKey)
                    }else{
                        NSLog("%@", "키체인에 저장가능한 형식이 아닙니다.")
                        self.setDefaultsValue(defaultValue, key: defaultKey, keyChain: false)
                    }
                }
            }else{
                assert(false, "키체인 그룹 설정")
            }
        }
    }
    
    
    /// NSUserDefaults 사용 단순화
    ///
    /// - Parameters:
    ///   - defaultKey: NSUserDefaults 에서 삭제할 값과 매칭되는 키
    ///   - keychain: 키체인 사용 여부
    public func removeDefaultsValue(_ defaultKey: String!, keychain : Bool = false) {
        if keychain == false {
            let defaults = UserDefaults.standard
            defaults.removeObject(forKey: defaultKey)
            defaults.synchronize()
        }else{
            let keychain = KeychainSwift()
            keychain.accessGroup = self._KEYCHAINGROUPID
            
            if self._KEYCHAINGROUPID.validate() {
                keychain.delete(defaultKey)
            }else{
                assert(false, "키체인 그룹 설정")
            }
            
        }
    }
    
    /// NSUserDefaults 사용 단순화, NSUserDefaults 싱크하기
    public func syncDefaults() {
        let defaults = UserDefaults.standard
        defaults.synchronize()
    }
    
    
    
    

    
    
    /// 이미지 불러오기
    ///
    /// - Parameter fileName: 불러올 파일이름
    /// - Returns: 불러온 이미지
    public func loadImageandName(_ fileName: String!) -> UIImage! {
        
        if let dir = try? FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false) {
            return UIImage(contentsOfFile: URL(fileURLWithPath: dir.absoluteString).appendingPathComponent(fileName).path)
        }
        return UIImage()
        
    }
    
    /// 파일 불러오기
    ///
    /// - Parameter fileName: 불러올 파일이름
    /// - Returns: 불러온 파일
    public func loadFileName(_ fileName: String!) -> Data! {
        let writeURL = URL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(fileName)
        do {
            return try Data.init(contentsOf: writeURL)
        } catch {
            return nil
        }
        
    }
    
    
    /// 사용자 파일 모두 삭제하기
    public func removeAllFiles() {
        let fileMgr = FileManager.default
        do {
            let files = try fileMgr.contentsOfDirectory(atPath: NSTemporaryDirectory())
            for filePath in files {
                try fileMgr.removeItem(atPath: filePath)
            }
        } catch  {
            return
        }
    }
    
    
    /// 파일 존재여부 검사
    ///
    /// - Parameter fileName: 체크할 파일이름
    /// - Returns: 존재여부
    public func fileExistChecker(_ fileName: String!) -> Bool {
        let filePath = URL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(fileName).path
        return FileManager.default.fileExists(atPath: filePath)
    }
    
    
    /// 파일 삭제
    ///
    /// - Parameter fileName: 삭제할 파일이름
    /// - Returns: 삭제성공
    public func fileRemover(_ fileName: String!) -> Bool {
        let filePath = URL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(fileName).path
        let fileMgr = FileManager.default
        
        do {
            try fileMgr.removeItem(atPath: filePath)
            return true
        } catch {
            return false
        }
    }
    
    

    
    
    /// Unique 문자열을 생성
    ///
    /// - Parameter length: 가져올 문자열의 길이
    /// - Returns: 유니크 문자열
    public func getUniqueString(_ length: Int) -> String! {
        let uuid = UUID().uuidString

        
        return String(uuid[..<uuid.index(uuid.startIndex, offsetBy: length)])
    }
    
    
    /// 시뮬레이터인지 판별
    ///
    /// - Returns: true 시뮬레이터 false 기기
    public func isSimulator() -> Bool {
        #if targetEnvironment(simulator)
        return true
        #else
        return false
        #endif
    }
    
    
    /// 아이폰 아이패드 구분
    ///
    /// - Returns: true - 아이패드
    public func isIpad() -> Bool {
        
        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad {
            return true
        }
        return false
    }
    
    
    /// 현재 화면 스크린 사이즈 가져오기
    ///
    /// - Returns: 화면 영역 크기
    public func getCurrentFrame() -> CGRect {
        return (UIApplication.shared.keyWindow?.frame)!
    }
    
    
    
    /// App 버젼 가져오기
    ///
    /// - Returns: plist 에서 정의된 어플 버젼 반환
    public func getAppVersion() -> String! {
        if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
            return version
        }else{
            return ""
        }
    }
    
    
    /// 빌드 버젼 가져오기
    ///
    /// - Returns: plist 에서 정의된 어플 빌드 버젼 반환
    public func getAppBuildVersion() -> String! {
        if let version = Bundle.main.infoDictionary?["CFBundleVersion"] as? String {
            return version
        }else{
            return ""
        }
    }
    
    /// 현재 버젼 가져오기
    ///
    /// - Returns: OS 버젼
    public func getOSVersion() -> String! {
        return UIDevice.current.systemVersion
    }
    
    
    /// App ID 가져오기
    ///
    /// - Returns: plist 에서 정의된 어플 ID 반환
    public func getAppBundleID() -> String! {
        return Bundle.main.bundleIdentifier
    }
    
    
    /// App 이름 가져오기
    ///
    /// - Returns: 어플 이름
    public func getAppName() -> String! {
        if let name = Bundle.main.infoDictionary?[kCFBundleNameKey as String] as? String {
            return name
        }else{
            return ""
        }
    }
    
    
    
    /// 1:1 비율 이미지 리사이즈
    ///
    /// - Parameters:
    ///   - oriImage: 원본
    ///   - width: 넓이
    ///   - backgroundTransparent: 배경 투명여부
    /// - Returns: 결과
    public func resizeImageBox(_ oriImage: UIImage, withSize width: Int, backgroundTransparent : Bool = false) -> UIImage {
        // Create a thumbnail image
        // resizedImage1View의 크기에 맞추어서 ori image을 resize한다.
        var imageSize = oriImage.size
        
        var ratio: CGFloat = 0
        if oriImage.size.width > oriImage.size.height {
            // 가로가 더 길면
            ratio = imageSize.width / CGFloat(width)
            imageSize.width = CGFloat(width)
            imageSize.height = imageSize.height * ratio
        }
        else {
            // 세로가 더 길면
            ratio = imageSize.height / CGFloat(width)
            imageSize.width = imageSize.width * ratio
            imageSize.height = CGFloat(width)

        }
        // image의 가로 세로가 다르기 때문에 ori image에 ratio를 적용해서 rect만들기
        //CGRect rect = CGRectMake(0, 0, ratio*oriImage.size.width, ratio*oriImage.size.height);
        return self.resizeImage(image: oriImage, targetSize: imageSize, backgroundTransparent : backgroundTransparent)
    }
    
    
    
    /// 이미지 리사이즈
    ///
    /// - Parameters:
    ///   - image: 원본 이미지
    ///   - targetSize: 리사이징 크기
    ///   - backgroundTransparent: 배경 투명 여부
    /// - Returns: 결과물
    public func resizeImage(image: UIImage, targetSize: CGSize, backgroundTransparent : Bool = false) -> UIImage {
        let size = image.size
        
        let widthRatio  = targetSize.width  / image.size.width
        let heightRatio = targetSize.height / image.size.height
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
        }
        
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        
        UIGraphicsBeginImageContextWithOptions(newSize, !backgroundTransparent, 1.0)
//        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        if !backgroundTransparent {
            //배경 흰색으로 채워줌
            UIColor.white.set()
            UIRectFill(rect)
        }
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    
    /// 이미지 컬러 마스킹
    ///
    /// - Parameters:
    ///   - tImage: 대상
    ///   - tColor: 변경 색상
    /// - Returns: 결과물
    public func convertColorMasking(_ tImage: UIImage, with tColor: UIColor) -> UIImage {
        let imageRect = CGRect(x: CGFloat(0.0), y: CGFloat(0.0), width: CGFloat(tImage.size.width), height: CGFloat(tImage.size.height))
        var newImage: UIImage? = nil
        UIGraphicsBeginImageContextWithOptions(imageRect.size, false, tImage.scale)
        do {
            let context: CGContext = UIGraphicsGetCurrentContext()!
            context.scaleBy(x: 1.0, y: -1.0)
            context.translateBy(x: 0.0, y: -(imageRect.size.height))
            context.clip(to: imageRect, mask: tImage.cgImage!)
            context.setFillColor(tColor.cgColor)
            context.fill(imageRect)
            newImage = UIGraphicsGetImageFromCurrentImageContext()
        }
        UIGraphicsEndImageContext()
        return newImage!
    }
    
    
    /// 이미지 흑백으로 변환
    ///
    /// - Parameter image: 원본 이미지
    /// - Returns: 결과물
    public func convertImageToBW(image:UIImage) -> UIImage {
        
        let filter = CIFilter(name: "CIPhotoEffectMono")
        
        // convert UIImage to CIImage and set as input
        
        let ciInput = CIImage(image: image)
        filter?.setValue(ciInput, forKey: "inputImage")
        
        // get output CIImage, render as CGImage first to retain proper UIImage scale
        
        let ciOutput = filter?.outputImage
        let ciContext = CIContext()
        let cgImage = ciContext.createCGImage(ciOutput!, from: (ciOutput?.extent)!)
        
        return UIImage(cgImage: cgImage!)
    }
    
    
    /// RGB 코드값으로 색상 반환
    ///
    /// - Parameter rgbValue: 코드값
    /// - Returns: 색상
    public func convert(rgbValue: UInt) -> UIColor {
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    


    
    /// 쿠키 청소
    public func cookieClear() {
        let storage = HTTPCookieStorage.shared
        let cookies : [HTTPCookie] = storage.cookies!
        //ap_web_guide_read 가 여러개 오면 안되니깐 하나 골라내서 저장
        
        for cookie in cookies {
            
            //            if cookie.name.contains("ap_web_guide_read") {
            //            }else{
            storage.deleteCookie(cookie)
            //            }
            
        }
        
        UserDefaults.standard.synchronize()
    }
   
    
    /// 캐쉬 청소
    public func cacheClear() {
        if #available(iOS 9.0, *)
        {
            let websiteDataTypes = NSSet(array: [WKWebsiteDataTypeDiskCache, WKWebsiteDataTypeMemoryCache])
            let date = NSDate(timeIntervalSince1970: 0)
            
            WKWebsiteDataStore.default().removeData(ofTypes: websiteDataTypes as! Set<String>, modifiedSince: date as Date, completionHandler:{ })
        }
        else
        {
            var libraryPath = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.libraryDirectory, FileManager.SearchPathDomainMask.userDomainMask, false).first!
            libraryPath += "/Cookies"
            
            do {
                try FileManager.default.removeItem(atPath: libraryPath)
            } catch {
                print("error")
            }
            URLCache.shared.removeAllCachedResponses()
        }
    }
    
    
    
    
    /// UILabel 에서 라인수 가져오기
    ///
    /// - Parameter label:
    /// - Returns:
    public func linesFromLabel(label: UILabel) -> Int {
        let textSize = CGSize(width: label.frame.size.width, height: CGFloat(Float.infinity))
        let rHeight = lroundf(Float(label.sizeThatFits(textSize).height))
        let charSize = lroundf(Float(label.font.lineHeight))
        let lineCount = rHeight/charSize
        return lineCount
    }
    
    
    /// 뺑글이
    public func showWaiting() {
        
//        let attributes = RappleActivityIndicatorView.attribute(style: RappleStyle.apple, thickness: 4)
//
//        RappleActivityIndicatorView.startAnimating(attributes: attributes)
        
        RappleActivityIndicatorView.startAnimating()
        
        delay(20) {
            
            RappleActivityIndicatorView.stopAnimation()
        }
    }
    
    
    /// 뺑글이 감추기
    public func hideWaiting() {
        RappleActivityIndicatorView.stopAnimation()

    }

    
    
    //WARNING: 그룹ID 수정필요
    open var _KEYCHAINGROUPID : String = ""
    
    //MARK: GPS
    var locationManager:CLLocationManager = CLLocationManager()
    var isUpdatingLocation = false
    func initGPS() {
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization() //권한 요청
        locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        
        if !self.isUpdatingLocation {
            locationManager.startUpdatingLocation()
        }
    }
    func stopGPS() {
        self.isUpdatingLocation = false
        self.locationManager.stopUpdatingLocation()
    }
    
    
    /// 사용자 현재 latitude
    var latitude : CLLocationDegrees = -1
    
    /// 사용자 현재 longitude
    var longitude : CLLocationDegrees = -1
    
    public func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        self.isUpdatingLocation = true
        if let coor = manager.location?.coordinate{
            self.latitude = coor.latitude
            self.longitude = coor.longitude
        }
    }
    
    
    open func takeScreenshot(_ shouldSave: Bool = false) -> UIImage? {
        var screenshotImage :UIImage?
        let layer = UIApplication.shared.keyWindow!.layer
        let scale = UIScreen.main.scale
        UIGraphicsBeginImageContextWithOptions(layer.frame.size, false, scale);
        guard let context = UIGraphicsGetCurrentContext() else {return nil}
        layer.render(in:context)
        screenshotImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        if let image = screenshotImage, shouldSave {
            UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
        }
        return screenshotImage
    }
    
    //SubView 존재할때 가장 하단에 위치한 뷰 포인트
    open func subviewBottom(_ targetView : UIView) -> CGFloat {
        var bottomPoint : CGFloat = 0
        for inView in targetView.subviews {
            let bottom = inView.frame.size.height + inView.frame.origin.y
            if bottom > bottomPoint {
                bottomPoint = bottom
            }
        }
        return bottomPoint
    }
    
    open func updateScrollViewContentSize(_ targetScrollView: UIScrollView, _ buffer : Int = 0) {
        var lowestPointX: CGFloat = 0.0
        var restoreHorizontal: Bool = false
        var restoreVertical: Bool = false
        if targetScrollView.responds(to: #selector(getter: targetScrollView.showsHorizontalScrollIndicator)) && targetScrollView.responds(to: #selector(getter: targetScrollView.showsVerticalScrollIndicator)) {
            if (targetScrollView.showsHorizontalScrollIndicator) {
                restoreHorizontal = true
                targetScrollView.showsHorizontalScrollIndicator = false
            }
            if (targetScrollView.showsVerticalScrollIndicator) {
                restoreVertical = true
                targetScrollView.showsVerticalScrollIndicator = false
            }
        }
        for subView: UIView in targetScrollView.subviews {
            if !subView.isHidden {
                let maxX: CGFloat = subView.frame.maxX
                if maxX > lowestPointX {
                    lowestPointX = maxX
                }
            }
        }
        if targetScrollView.responds(to: #selector(getter: targetScrollView.showsHorizontalScrollIndicator)) && targetScrollView.responds(to: #selector(getter: targetScrollView.showsVerticalScrollIndicator)) {
            if restoreHorizontal {
                targetScrollView.showsHorizontalScrollIndicator = true
            }
            if restoreVertical {
                targetScrollView.showsVerticalScrollIndicator = true
            }
        }
        
        var lowestPointY: CGFloat = 0.0
        restoreHorizontal = false
        restoreVertical = false
        if targetScrollView.responds(to: #selector(getter: targetScrollView.showsHorizontalScrollIndicator)) && targetScrollView.responds(to: #selector(getter: targetScrollView.showsVerticalScrollIndicator)) {
            if (targetScrollView.showsHorizontalScrollIndicator) {
                restoreHorizontal = true
                targetScrollView.showsHorizontalScrollIndicator = false
            }
            if (targetScrollView.showsVerticalScrollIndicator) {
                restoreVertical = true
                targetScrollView.showsVerticalScrollIndicator = false
            }
        }
        for subView: UIView in targetScrollView.subviews {
            if !subView.isHidden && subView.tag != 9393 {
                let maxY: CGFloat = subView.frame.maxY
                if maxY > lowestPointY {
                    lowestPointY = maxY
                }
            }
        }
        if targetScrollView.responds(to: #selector(getter: targetScrollView.showsHorizontalScrollIndicator)) && targetScrollView.responds(to: #selector(getter: targetScrollView.showsVerticalScrollIndicator)) {
            if restoreHorizontal {
                targetScrollView.showsHorizontalScrollIndicator = true
            }
            if restoreVertical {
                targetScrollView.showsVerticalScrollIndicator = true
            }
        }
        targetScrollView.contentSize = CGSize(width: lowestPointX + CGFloat(buffer), height: lowestPointY + CGFloat(buffer))
    }
    
    open func updateScrollViewContentHeight(_ targetScrollView: UIScrollView, _ buffer : Int = 0) {
        //헐 이건 스크롤 인디케이터도 포함;;
        //    CGRect contentRect = CGRectZero;
        //    for (UIView *view in targetScrollView.subviews) {
        //        contentRect = CGRectUnion(contentRect, view.frame);
        //    }
        //    targetScrollView.contentSize = CGSizeMake(0, contentRect.size.height);
        //이건 잘됨
        
        var lowestPoint: CGFloat = 0.0
        var restoreHorizontal: Bool = false
        var restoreVertical: Bool = false
        if targetScrollView.responds(to: #selector(getter: targetScrollView.showsHorizontalScrollIndicator)) && targetScrollView.responds(to: #selector(getter: targetScrollView.showsVerticalScrollIndicator)) {
            if (targetScrollView.showsHorizontalScrollIndicator) {
                restoreHorizontal = true
                targetScrollView.showsHorizontalScrollIndicator = false
            }
            if (targetScrollView.showsVerticalScrollIndicator) {
                restoreVertical = true
                targetScrollView.showsVerticalScrollIndicator = false
            }
        }
        for subView: UIView in targetScrollView.subviews {
            if !subView.isHidden && subView.tag != 9393 {
                let maxY: CGFloat = subView.frame.maxY
                if maxY > lowestPoint {
                    lowestPoint = maxY
                }
            }
        }
        if targetScrollView.responds(to: #selector(getter: targetScrollView.showsHorizontalScrollIndicator)) && targetScrollView.responds(to: #selector(getter: targetScrollView.showsVerticalScrollIndicator)) {
            if restoreHorizontal {
                targetScrollView.showsHorizontalScrollIndicator = true
            }
            if restoreVertical {
                targetScrollView.showsVerticalScrollIndicator = true
            }
        }
        targetScrollView.contentSize = CGSize(width: targetScrollView.frame.size.width, height: lowestPoint + CGFloat(buffer))
    }
    
    open func updateScrollViewContentWidth(_ targetScrollView: UIScrollView, _ buffer : Int = 0) {
        //    CGRect contentRect = CGRectZero;
        //    for (UIView *view in targetScrollView.subviews) {
        //        contentRect = CGRectUnion(contentRect, view.frame);
        //    }
        //    targetScrollView.contentSize = CGSizeMake(contentRect.size.width, 0);
        var lowestPoint: CGFloat = 0.0
        var restoreHorizontal: Bool = false
        var restoreVertical: Bool = false
        if targetScrollView.responds(to: #selector(getter: targetScrollView.showsHorizontalScrollIndicator)) && targetScrollView.responds(to: #selector(getter: targetScrollView.showsVerticalScrollIndicator)) {
            if (targetScrollView.showsHorizontalScrollIndicator) {
                restoreHorizontal = true
                targetScrollView.showsHorizontalScrollIndicator = false
            }
            if (targetScrollView.showsVerticalScrollIndicator) {
                restoreVertical = true
                targetScrollView.showsVerticalScrollIndicator = false
            }
        }
        for subView: UIView in targetScrollView.subviews {
            if !subView.isHidden && subView.tag != 9393 {
                let maxX: CGFloat = subView.frame.maxX
                if maxX > lowestPoint {
                    lowestPoint = maxX
                }
            }
        }
        if targetScrollView.responds(to: #selector(getter: targetScrollView.showsHorizontalScrollIndicator)) && targetScrollView.responds(to: #selector(getter: targetScrollView.showsVerticalScrollIndicator)) {
            if restoreHorizontal {
                targetScrollView.showsHorizontalScrollIndicator = true
            }
            if restoreVertical {
                targetScrollView.showsVerticalScrollIndicator = true
            }
        }
        if targetScrollView.isPagingEnabled {
            //페이징이면 페이지 단위로 넓이 지정
            let margin = lowestPoint.truncatingRemainder(dividingBy: targetScrollView.frame.size.width)
            if margin > 0 {
                targetScrollView.contentSize = CGSize(width: lowestPoint + CGFloat(buffer) + (targetScrollView.frame.size.width - margin), height: targetScrollView.frame.size.height)
            }else{
                targetScrollView.contentSize = CGSize(width: lowestPoint + CGFloat(buffer), height: targetScrollView.frame.size.height)
            }
            
        }else{
            targetScrollView.contentSize = CGSize(width: lowestPoint + CGFloat(buffer), height: targetScrollView.frame.size.height)
        }
        
    }
    //내용들을 감지해서 ScrollView 보다 넓이다 좁으면 가운데로 이동 시켜줌
    open func updateScrollViewContentWidthMakeCenter(_ targetScrollView: UIScrollView, _ buffer : Int = 0) {
        //    CGRect contentRect = CGRectZero;
        //    for (UIView *view in targetScrollView.subviews) {
        //        contentRect = CGRectUnion(contentRect, view.frame);
        //    }
        //    targetScrollView.contentSize = CGSizeMake(contentRect.size.width, 0);
        var lowestPoint: CGFloat = 0.0
        var restoreHorizontal: Bool = false
        var restoreVertical: Bool = false
        if targetScrollView.responds(to: #selector(getter: targetScrollView.showsHorizontalScrollIndicator)) && targetScrollView.responds(to: #selector(getter: targetScrollView.showsVerticalScrollIndicator)) {
            if (targetScrollView.showsHorizontalScrollIndicator) {
                restoreHorizontal = true
                targetScrollView.showsHorizontalScrollIndicator = false
            }
            if (targetScrollView.showsVerticalScrollIndicator) {
                restoreVertical = true
                targetScrollView.showsVerticalScrollIndicator = false
            }
        }
        for subView: UIView in targetScrollView.subviews {
            if !subView.isHidden && subView.tag != 9393 {
                let maxX: CGFloat = subView.frame.maxX
                if maxX > lowestPoint {
                    lowestPoint = maxX
                }
            }
        }
        if targetScrollView.responds(to: #selector(getter: targetScrollView.showsHorizontalScrollIndicator)) && targetScrollView.responds(to: #selector(getter: targetScrollView.showsVerticalScrollIndicator)) {
            if restoreHorizontal {
                targetScrollView.showsHorizontalScrollIndicator = true
            }
            if restoreVertical {
                targetScrollView.showsVerticalScrollIndicator = true
            }
        }
        if targetScrollView.isPagingEnabled {
            //페이징이면 페이지 단위로 넓이 지정
            let margin = lowestPoint.truncatingRemainder(dividingBy: targetScrollView.frame.size.width)
            if margin > 0 {
                targetScrollView.contentSize = CGSize(width: lowestPoint + CGFloat(buffer) + (targetScrollView.frame.size.width - margin), height: targetScrollView.frame.size.height)
            }else{
                targetScrollView.contentSize = CGSize(width: lowestPoint + CGFloat(buffer), height: targetScrollView.frame.size.height)
            }
            
        }else{
            
            if targetScrollView.frame.size.width > lowestPoint {
                let margin = targetScrollView.frame.size.width - lowestPoint
                //                targetScrollView.contentSize = CGSize(width: lowestPoint + CGFloat(buffer), height: targetScrollView.frame.size.height)
                targetScrollView.isUserInteractionEnabled = false
                targetScrollView.setContentOffset(CGPoint(x: -(margin/2), y: 0), animated: false)
                
            }else{
                targetScrollView.contentSize = CGSize(width: lowestPoint + CGFloat(buffer), height: targetScrollView.frame.size.height)
            }
            
        }
        
    }
    
    
    
}


extension NSAttributedString {
    func heightWithConstrainedWidth(width: CGFloat) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, context: nil)
        
        return boundingBox.height
    }
    
    func widthWithConstrainedHeight(height: CGFloat) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, context: nil)
        
        return boundingBox.width
    }
}



/// 다용도 디버그 로그
public func EVLog<T>(_ object: T, filename: String = #file, line: Int = #line, funcname: String = #function) {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "MM/dd/yyyy HH:mm:ss:SSS"
    let process = ProcessInfo.processInfo
    let threadId = "?"
    
    print("\(dateFormatter.string(from: Date())) \(process.processName))[\(process.processIdentifier):\(threadId)](\(line)) \(String(describing: filename.extractFileName()))!):\(funcname):\r\t\(object)\n")
}

public func delay(_ delay:Double, closure:@escaping ()->()) {
    DispatchQueue.main.asyncAfter(
        deadline: DispatchTime.now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: closure)
    
}

public func degreessToRadians(_ degree : Double) -> Double {
    return ((Double.pi * degree) / 180.0)
}

public extension Sequence {
    
    /// 배열 안에서 해당 값으로 그룹 지어서 반환
    ///
    /// - Parameter key:
    /// - Returns:
    func group<U: Hashable>(by key: (Iterator.Element) -> U) -> [U:[Iterator.Element]] {
        var categories: [U: [Iterator.Element]] = [:]
        for element in self {
            let key = key(element)
            if case nil = categories[key]?.append(element) {
                categories[key] = [element]
            }
        }
        return categories
    }
}



@IBDesignable
class ExpandButton: UIButton {
    
    @IBInspectable var margin:CGFloat = 20.0
    override func point(inside point: CGPoint, with  event: UIEvent?) -> Bool {
        let relativeFrame = self.bounds
        let hitTestEdgeInsets = UIEdgeInsets(top: -margin, left: -margin, bottom: -margin, right: -margin)
        let hitFrame = relativeFrame.inset(by: hitTestEdgeInsets)
        return hitFrame.contains(point)
    }
    
}

@IBDesignable
class InsetsTextField: UITextField {
    
    @IBInspectable var margin:CGFloat = 10.0
        
    // placeholder position
    override func textRect(forBounds bounds: CGRect) -> CGRect {

        return bounds.insetBy(dx: margin, dy: 0)
    }
    
    // text position
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: margin, dy: 0)
    }
}

class DashedBorderView: UIView {
    
    let _border = CAShapeLayer()
    var isHorizontal = true
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup(nil)
    }
    
    init(_ dashColor : UIColor?, _ aisHorizontal : Bool = true) {
        super.init(frame: CGRect.zero)
        self.isHorizontal = aisHorizontal
        setup(dashColor)
    }
    
    func setup(_ dashColor : UIColor?) {
        _border.strokeColor = dashColor?.cgColor ?? "333333".convertToColor().cgColor
        _border.fillColor = nil
        _border.lineDashPattern = [2, 2]
        self.layer.addSublayer(_border)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        let linePath = UIBezierPath()
        if isHorizontal {
            linePath.move(to: CGPoint.zero)
            linePath.addLine(to: CGPoint(x: self.bounds.size.width, y: 0))
        }else{
            linePath.move(to: CGPoint.zero)
            linePath.addLine(to: CGPoint(x: 0, y: self.bounds.size.height))
        }
        _border.path = linePath.cgPath
//
//        _border.path = UIBezierPath(roundedRect: CGRect(x: 0, y: 0, width: self.bounds.size.width, height: 0), cornerRadius:0).cgPath
//
//
//        _border.frame = self.bounds
    }
}
class DashedBorderBoxView: UIView {
    
    let _border = CAShapeLayer()
    var isHorizontal = true
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup(nil)
    }
    
    init(_ dashColor : UIColor) {
        super.init(frame: CGRect.zero)
        setup(dashColor)
    }
    
    func setup(_ dashColor : UIColor?) {
        _border.strokeColor = dashColor?.cgColor ?? "333333".convertToColor().cgColor
        _border.fillColor = nil
        _border.lineDashPattern = [4, 4]
        self.layer.addSublayer(_border)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()

        _border.path = UIBezierPath(roundedRect: CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.height), cornerRadius:0).cgPath
        _border.frame = self.bounds
    }
}
extension UIView {
    
    // Using a function since `var image` might conflict with an existing variable
    // (like on `UIImageView`)
    public func asImage() -> UIImage {
        if #available(iOS 10.0, *) {
            let renderer = UIGraphicsImageRenderer(bounds: bounds)
            return renderer.image { rendererContext in
                layer.render(in: rendererContext.cgContext)
            }

        } else {
            // Fallback on earlier versions
            UIGraphicsBeginImageContext(self.frame.size)
            self.layer.render(in:UIGraphicsGetCurrentContext()!)
            let image = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            return UIImage(cgImage: image!.cgImage!)
        }
    }
    
    public func takeSnapshotOfView() -> UIImage? {
        UIGraphicsBeginImageContext(CGSize(width: self.frame.size.width, height: self.frame.size.height))
        self.drawHierarchy(in: CGRect(x: 0.0, y: 0.0, width: self.frame.size.width, height: self.frame.size.height), afterScreenUpdates: true)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
}

/*
 https://github.com/vinbhai4u/CLImageViewPopup
 */
class CLImageViewPopup: UIImageView {
    var tempRect: CGRect?
    var bgView: UIView!
    
    var animated: Bool = true
    var intDuration = 0.25
    //MARK: Life cycle
    override func draw(_ rect: CGRect) {
        super.draw(rect)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(CLImageViewPopup.popUpImageToFullScreen))
        self.addGestureRecognizer(tapGesture)
        self.isUserInteractionEnabled = true
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(CLImageViewPopup.popUpImageToFullScreen))
        self.addGestureRecognizer(tapGesture)
        self.isUserInteractionEnabled = true
        //        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: Actions of Gestures
    @objc func exitFullScreen () {
        let imageV = bgView.subviews[0] as! UIImageView
        
        UIView.animate(withDuration: intDuration, animations: {
            imageV.frame = self.tempRect!
            self.bgView.alpha = 0
        }, completion: { (bol) in
            self.bgView.removeFromSuperview()
        })
    }
    
    @objc func popUpImageToFullScreen() {
        
        if let window = UIApplication.shared.delegate?.window {
            let parentView = self.findParentViewController(self)!.view
            
            bgView = UIView(frame: UIScreen.main.bounds)
            bgView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(CLImageViewPopup.exitFullScreen)))
            bgView.alpha = 0
            bgView.backgroundColor = UIColor.black
            let imageV = UIImageView(image: self.image)
            let point = self.convert(self.bounds, to: parentView)
            imageV.frame = point
            tempRect = point
            imageV.contentMode = .scaleAspectFit
            self.bgView.addSubview(imageV)
            window?.addSubview(bgView)
            
            if animated {
                UIView.animate(withDuration: intDuration, animations: {
                    self.bgView.alpha = 1
                    imageV.frame = CGRect(x: 0, y: 0, width: (parentView?.frame.width)!, height: (parentView?.frame.width)!)
                    imageV.center = (parentView?.center)!
                })
            }
        }
    }
    
    func findParentViewController(_ view: UIView) -> UIViewController? {
        var parentResponder: UIResponder? = self
        while parentResponder != nil {
            parentResponder = parentResponder!.next
            if let viewController = parentResponder as? UIViewController {
                return viewController
            }
        }
        return nil
    }
    
}

public enum StringToDateConvertingType {
    case yyyy
    case yyyymmdd
    case yyyyMMddHHmm
}

public enum BorderDirection {
    case top
    case bottom
    case left
    case right
    case all
}

