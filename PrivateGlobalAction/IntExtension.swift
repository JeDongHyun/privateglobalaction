//
//  IntExtension.swift
//  PrivateGlobalAction
//
//  Created by Je Dong hyun on 15/05/2019.
//  Copyright © 2019 Dongs. All rights reserved.
//

import UIKit

public extension Int {
    func withCommas() -> String {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = NumberFormatter.Style.decimal
        return numberFormatter.string(from: NSNumber(value:self))!
    }
}
