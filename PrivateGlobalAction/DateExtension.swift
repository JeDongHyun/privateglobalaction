//
//  DateExtension.swift
//  PrivateGlobalAction
//
//  Created by gls on 2018. 7. 2..
//  Copyright © 2018년 Dongs. All rights reserved.
//

import UIKit

public extension Date {
    /// Date 형식의 데이터를 문자열로
    ///
    /// - Parameters:
    ///   - Format: YYYYMMDD 같은 문자열 형식 전달
    /// - Returns: 변환된 날짜 문자열
    func convertDate(toStr Format: String!) -> String! {
        let formatter = DateFormatter()
        formatter.dateFormat = Format
        
        return formatter.string(from: self)
    }
    
    func addingDate(addbyUnit:Calendar.Component, value : Int) -> Date {
        
        var date = self
        let endDate = Calendar.current.date(byAdding: addbyUnit, value: value, to: date)!
        return endDate
    }
    func generateDates(startDate :Date?, addbyUnit:Calendar.Component, value : Int) -> [Date] {
        
        var dates = [Date]()
        var date = startDate!
        let endDate = Calendar.current.date(byAdding: addbyUnit, value: value, to: date)!
        while date < endDate {
            date = Calendar.current.date(byAdding: addbyUnit, value: 1, to: date)!
            dates.append(date)
        }
        return dates
    }
}
