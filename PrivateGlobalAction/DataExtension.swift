//
//  DataExtension.swift
//  PrivateGlobalAction
//
//  Created by gls on 2018. 7. 2..
//  Copyright © 2018년 Dongs. All rights reserved.
//

import UIKit

public extension Data {
    /// 파일 저장
    ///
    /// - Parameters:
    ///   - fileData: 저장할 파일 데이터
    ///   - fileName: 저장할 파일 이름
    func saveFile(name fileName: String!) {
        if fileName.validate() == false {
            return
        }
        
        let writeURL = URL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(fileName)
        do {
            try self.write(to: writeURL)
        } catch {
            NSLog("%@", "이미지 파일 쓰기 실패")
        }
        
    }
    
    /// HTML 태그를 AttributedString 으로
    var html2AttributedString: NSAttributedString? {
        do {
            return try NSAttributedString(data: self, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            print("error:", error)
            return  nil
        }
    }
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
    
}
