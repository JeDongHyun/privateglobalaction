//
//  PrivateGlobalAction.h
//  PrivateGlobalAction
//
//  Created by gls on 2018. 6. 29..
//  Copyright © 2018년 Dongs. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for PrivateGlobalAction.
FOUNDATION_EXPORT double PrivateGlobalActionVersionNumber;

//! Project version string for PrivateGlobalAction.
FOUNDATION_EXPORT const unsigned char PrivateGlobalActionVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <PrivateGlobalAction/PublicHeader.h>

